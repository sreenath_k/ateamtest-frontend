import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';
import { CookieService } from 'ngx-cookie-service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	users: Array<User> = [];
	friends: Array<User> = [];

  constructor(
  	private router: Router,
    private authenticationService: AuthenticationService,
    private cookieService: CookieService,
    private userService: UserService,
  	) { }

  ngOnInit() {
  	this.authenticationService.authenticateUser()
  		.subscribe(response => {
  			if (response.status == true) {
  				this.getUsers();
  				this.getFriends();
  			}
  			else{
  				this.cookieService.delete('userToken');
				localStorage.removeItem('user');
  				this.router.navigate(['/login']);
  			}
  		})
  }
  getUsers(){
  	this.userService.getUsers()
		.subscribe(response => {
		if (response.status == true) {
			this.users = response.data;
		}
		else {
			this.users = [];
		}
	});
  }
  getFriends(){
  	this.userService.getFriends()
		.subscribe(response => {
		if (response.status == true) {
			this.friends = response.data;
		}
		else {
			this.friends = [];
		}
	});
  }
  sendRequest(id){
  	console.log(id)
  	this.userService.sendRequest(id)
		.subscribe(response => {
		if (response.status == true) {
			//this.users = response.data;
		}
		else {
			//this.users = [];
		}
	});
  }
  userLogout(){
  	this.authenticationService.userLogout(this.cookieService.get('userToken'))
		.subscribe(response => {
		if (response.status == true) {
			this.cookieService.delete('userToken');
			localStorage.removeItem('user');
			this.router.navigate(['/login']);
		}
		else {
			
		}
	});
  }
}
