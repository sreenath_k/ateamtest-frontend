import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CookieService } from 'ngx-cookie-service';

import { Login } from '../../../models/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	login: Login = {
		email: '',
		password: ''
	};
	isEmailError: boolean = false;
	isPasswordError: boolean = false;
	isBtnDisabled: boolean = false;
	loginStatusMessage: string = '';

	constructor(
		private router: Router,
    	private authenticationService: AuthenticationService,
    	private cookieService: CookieService
    	) { }

	ngOnInit() {
	}

	emailChange():void{
	    if(this.login.email == ''){
	     	this.isEmailError = true;
	    }
	    else{
	    	this.isEmailError = false;
	    }
	}
	passwordChange():void{
	    if(this.login.password == ''){
	     	this.isPasswordError = true;
	    }
	    else{
	    	this.isPasswordError = false;
	    }
	}
	validate(){
		let error = false;
		if(this.login.email == ''){
			this.isEmailError = true;
			error = true;
		}
		if(this.login.password == ''){
			this.isPasswordError = true;
			error = true;
		}
		if(error){
			return false;
		}
		else{
			return true;
		}
	}
	userLogin():void{
		if(this.validate()){
			this.isBtnDisabled = true;
			this.authenticationService.userLogin(this.login)
				.subscribe(response => {
				if (response.status == true) {
					this.cookieService.set( 'userToken', response.data.api_token );
					localStorage.setItem('user', JSON.stringify(response.data));
					this.router.navigate(['/user/home']);
				}
				else {
					this.loginStatusMessage = response.message;
				}
				this.isBtnDisabled = false;
			});
		}
		else{
			
		}
	}

}
