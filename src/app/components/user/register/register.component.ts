import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  	user: User = {
  		name: '',
		email: '',
		password: '',
		gender: 'm',
		dp: null
	};
	confirmPassword: '';
	isNameError: boolean = false;
	isEmailError: boolean = false;
	isPasswordError: boolean = false;
	isPasswordMismatchError: boolean = false;
	isDpError: boolean = false;
	isBtnDisabled: boolean = false;

	constructor(
		private router: Router,
    	private authenticationService: AuthenticationService
    	) { }

	ngOnInit() {
	}

	nameChange():void{
	    if(this.user.name == ''){
	     	this.isNameError = true;
	    }
	    else{
	    	this.isNameError = false;
	    }
	}
	emailChange():void{
	    if(this.user.email == ''){
	     	this.isEmailError = true;
	    }
	    else{
	    	this.isEmailError = false;
	    }
	}
	passwordChange():void{
	    if(this.user.password == ''){
	     	this.isPasswordError = true;
	    }
	    else{
	    	this.isPasswordError = false;
	    }
	}
	checkPassword(){
		if(this.user.password != this.confirmPassword){
			return false;
		}
		return true;
	}
	dpChangeEvent(event){
		this.user.dp = event.target.files[0];
		console.log(this.user.dp);
	}
	validate(){
		let error = false;
		if(this.user.name == ''){
			this.isNameError = true;
			error = true;
		}
		else{
			this.isNameError = false;
		}
		if(this.user.email == ''){
			this.isEmailError = true;
			error = true;
		}
		else{
			this.isEmailError = false;
		}
		if(this.user.password == ''){
			this.isPasswordError = true;
			error = true;
		}
		else{
			this.isPasswordError = false;
		}
		if(!this.checkPassword()){
			this.isPasswordMismatchError = true;
			error = true;
		}
		else{
			this.isPasswordMismatchError = false;
		}
		if(this.user.dp == null){
			this.isDpError = true;
			error = true;
		}
		else{
			this.isDpError = false;
		}
		if(error){
			return false;
		}
		else{
			return true;
		}
	}
	userRegister():void{
		if(this.validate()){
			this.isBtnDisabled = true;
			this.authenticationService.userRegister(this.user)
				.subscribe(response => {
				if (response.status == true) {
					//do
				}
				else {
				}
				this.isBtnDisabled = false;
			});
		}
		else{
			alert('Not');
		}
	}

}
