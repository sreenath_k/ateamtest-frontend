import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../models/user';
import { Response } from '../models/response';
import { Login } from '../models/login';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private baseURL = environment.API_URL;

	photo: File;
	imageArr: Array<any> = [];

	constructor(
		private http: HttpClient,
		private cookieService: CookieService
	) { }

	userLogin(login: Login): Observable<Response> {
		let params = this.makeFormBody(login);
		return this.http.post<Response>(this.baseURL + 'userLogin', params, httpOptions);
	}

	userRegister(user: User): Observable<Response> {
		let formData: FormData = this.makeFormData(user);
		let headers = new HttpHeaders();
		headers.append('Content-Type', 'multipart/form-data');
		headers.append('Accept', 'application/json');
		let options = { headers: headers };
		return this.http.post<Response>(this.baseURL + 'userRegister', formData, options);
	}
	userLogout(token: string): Observable<Response> {
		let params = this.makeFormBody({token: token});
		return this.http.post<Response>(this.baseURL + 'userLogout', params, httpOptions);
	}
	authenticateUser(): Observable<Response> {
		httpOptions.headers = httpOptions.headers.set('Authorization', this.cookieService.get('userToken'));
		return this.http.post<Response>(this.baseURL + 'authenticateUser', '', httpOptions);
	}

	makeFormBody(params){
        let formBody = '';
        for (let property in params) {
            let encodedKey = encodeURIComponent(property);
            let encodedValue = encodeURIComponent(params[property]);
            formBody += encodedKey + "=" + encodedValue + "&";
        }
        //formBody.join("&");
        return formBody;
    }
    makeFormData(params){
        let formBody = new FormData();
        for(let property in params){
            formBody.append(property, params[property]);
        }
        return formBody;
    }
}
