import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../models/user';
import { Response } from '../models/response';
import { Login } from '../models/login';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

   private baseURL = environment.API_URL;

	constructor(
		private http: HttpClient,
		private cookieService: CookieService
	) { }

	getUsers(): Observable<Response> {
		httpOptions.headers = httpOptions.headers.set('Authorization', this.cookieService.get('userToken'));
		return this.http.get<Response>(this.baseURL + 'getUsers', httpOptions);
	}
	getFriends(): Observable<Response> {
		httpOptions.headers = httpOptions.headers.set('Authorization', this.cookieService.get('userToken'));
		return this.http.get<Response>(this.baseURL + 'getFriends', httpOptions);
	}
	sendRequest(id): Observable<Response> {
		let params = 'friendId='+id;
		httpOptions.headers = httpOptions.headers.set('Authorization', this.cookieService.get('userToken'));
		return this.http.post<Response>(this.baseURL + 'sendRequest', params, httpOptions);
	}
}